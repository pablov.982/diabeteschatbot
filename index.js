var express = require("express");
var bodyparser = require("body-parser");
const ngrok = require("ngrok")

var app = express();

var puerto = process.env.PORT || 3000;
var ip = process.env.IP || "127.0.0.1";

app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

app.post('/', function (req, res) {
    if (req.body.queryResult.action == "diabetes") {
        let diabetes = req.body.queryResult.parameters.diabetes;
        let sintomas = req.body.queryResult.parameters.sintomas;

        if (diabetes == "diabetes") {
            response = "La Diabetes es una enfermedad";
        }

        else if (sintomas == "sintomas") {
            response = "Los " + sintomas + " son dolor de estomago";
        }


        console.log('resultado', response);
        res.json({
            "fulfillmentText": response
        });
    }
    else if (req.body.queryResult.action == "Saludo") {
        let saludo = req.body.queryResult.parameters.Hola;
        if (saludo == "Hola") {
            response = "Hola! Para nosotros es un gusto atenderle, para brindarle una atención personalizada por favor ingrese su cédula: ";
        }
        console.log('resultado', response);
        res.json({
            "fulfillmentText": response
        });
    }
    
    else if (req.body.queryResult.action == "input.unknown") {
        console.log('entro a action desconocido');
        response = "No entiendo tu pregunta, podrias realizarla nuevamente por favor";
        res.json({
            "fulfillmentText": response
        });
    }
});

app.listen(puerto, ip);
(async function () {
    const url = await ngrok.connect(puerto);
    console.log(url);
})();